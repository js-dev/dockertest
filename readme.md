## Docker Dev Env

with the following containers:

* deb
    * apache
    * php5.6
    * composer
    * nodejs
    * grunt
    * bower
    * coffee-script
* mysql 
* solr
* redis


## Usage 


# preinstall 
install docker on your host. 


# install 
clone this docker repository, go to the directory and create the docker containers:
```bash
$ git clone https://bitbucket.org/js-dev/dockertest
$ cd dockertest
$ sh build.sh
```


# run
run a shell script and cocker-compose starts your containers:
```bash 
$ sh start.sh
```
edit host file on your host maschine and add demo.dev:
```bash 
$ sudo echo "127.0.0.1 demo.dev" >> /etc/hosts
``` 
open your browser and open: http://demo.dev and you should see a hello world example. 


# working with docker
- open a shell on the deb container: 
```bash 
$ sh bash.sh
```
- docker use env vars, with that you can connect from one container to other container. 
- connect to mysql from deb container with (instead of localhost): $DB_PORT_3306_TCP_ADDR
- you can use mysql workbench on your host system and connect with: localhost:3306; usr: root; pw: vagrant
- add/edit vhost: vhosts stored in ./deb/config; make your changes there and run: 
```bash 
sh stop.sh && sh start.sh
```

## docker volumes
for data persistance and access from host system you can use these volumes:

* the /var/www: ./www
* apache logs: ./deb/logs
* apache vhosts: ./deb/config
* folder for mysql dumps: ./mysql/dumps

accessing logfiles:

* apache logs: ./deb/logs
* mysql logs: ./mysql/logs

persistent database folders (dont touch this folders):

* mysql databases: ./mysql/db
* solr cores: ./solr/data
* redis storage: ./redis/data


### TODO
* add php pecl solr extension
* upgrade to php7 (also ubuntu distribution)
* upgrade to nodejs 6.x (after upgrading ubuntu)
* add nginx as reverse proxy